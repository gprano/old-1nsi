# TP : exploiter des données ouvertes

L’INSEE est l’institut chargé de la production, de l’analyse et de la publication des statistiques officielles en France.
Nombre de ces données sont accessibles librement sur son site, par exemple à l’adresse 
[https://www.insee.fr/fr/statistiques/4994589](https://www.insee.fr/fr/statistiques/4994589) 
on peut télécharger un fichier CSV de 2.3 millions de ligne,
chacune listant des informations sur une personne salariée (une personne sur 12 est dans le fichier).

Pour faciliter un peu le traitement, je vous ai préparé le fichier [salaires_2017_20k.csv](salaires_2017_20k.csv) 
qui contient seulement 20000 lignes prises au hasard, mais une fois que votre code fonctionne vous pouvez le tester
sur le fichier entier.

!!! list "À faire"
    Ouvrez dans Geany le fichier `salaires_2017_20k.csv`. Les noms des champs sur la première
    ligne ainsi que les valeurs de ces champs sur les lignes suivantes sont un peu mystérieux, et
    sont détaillés dans un autre fichier CSV : [Varmod_SALAAN_2017.csv](Varmod_SALAAN_2017.csv).
    
    En regardant les deux fichiers, trouvez dans quelle tranche est le salaire annuel de la personne
    de la première ligne.

Ouvrez ensuite Thonny, et enregistrez un script python dans le même dossier que le fichier csv.

Lorsqu'on lit un très gros fichier, on préfère ne pas convertir directement le fichier en liste python car c'est possible qu'il ne tienne pas dans la RAM de l'ordinateur. Au lieu de convertir le résultat de `csv.DictReader()` directement avec `list()`, on peut le parcourir ligne par ligne avec une boucle for :

```python
import csv

fichier = open("salaires_2017_20k.csv")
reader = csv.DictReader(fichier,delimiter=";")

nb_femmes = 0
for ligne in reader:
    if ligne["SEXE"]=="2":
        nb_femmes += 1
print(nb_femmes)
```

!!! danger "Attention"
    On ne peut pas parcourir deux fois de suite le fichier de cette manière car lire chaque ligne la "consomme", et la deuxième fois il n'y en aura plus. Il faut dans ce cas remettre la lecture du fichier au début avec `fichier.seek(0)`
    
!!! list "À faire"
    Ajoutez une variable `nb_hommes` au code précédent, et mettez là à jour dans
    la même boucle for pour afficher à la fin le nombre de femmes et d'hommes dans
    ce fichier.
    
Le champ qui correspond au salaire s'appelle **TRNNETO** et prend des valeurs entre
0 et 23.
    
!!! list "À faire"
    En ajoutant une condition, trouvez le nombre de femmes du fichiers qui ont un
    salaire dans la tranche la plus élevée (23).
    Trouvez de même le nombre d'hommes qui ont un salaire dans cette tranche.

On voudrait avoir une vue d'ensemble pour chacune des tranches salariales : 

* Créez deux listes, salairesF et salairesH de 24 éléments chacune, initialisées à 0.
`salairesF[t]` contiendra le nombre de femmes dont le salaire est dans la tranche `t`.
* Dans la boucle for, mettez dans une variable `t` la tranche salariale de la personne
de la ligne actuelle, convertie en int.
* Puis selon le cas, incrémentez `salairesF[t]` ou `salairesH[t]`
* Affichez le contenu de `salairesF` et `salairesH`

Pour voir ça sous forme graphique, on peut utiliser la bibliothèque python `matplotlib` qui permet de tracer des graphiques (et `numpy` que vous verrez parfois et qui sert
à manipuler des données avec plus de fonctionnalités que les listes) :

```python
import matplotlib.pyplot as plt
import numpy as np

def hbar_plot(l1, l2):
    """Affiche les données de l1 et l2 en barres"""
    n = len(l1)
    x = np.arange(n)
    plt.barh(x,l2,0.8,color="gray",alpha=0.7,label="l2")
    plt.barh(x,l1,0.8,color="purple",alpha=0.5,label="l1")
    plt.xlabel("nombre de personnes")
    plt.ylabel("catégorie de salaires")
    plt.legend()
    plt.show()
```

Copiez ce code et utilisez la fonction `hbar_plot` pour afficher visuellement
le contenu de `salairesF` et `salairesH`.

Que peut-on en conclure sur l'égalité salariale entre les femmes et les hommes en
France ?

!!! list "À faire"
    Pour vérifier l'analyse, téléchargez le fichier complet sur le site de l'INSEE
    et faites tourner le code sur les 2.3 millions de lignes. 
    
    Combien de temps cela prend-il ?
    
!!! list "À faire"
    Observer le fichier de description des champs pour bien voir les variables à votre
    disposition. 
    
    Formulez une hypothèse qui vous intéresse et modifier votre code
    pour la tester !
