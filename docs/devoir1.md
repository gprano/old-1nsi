# Devoir de NSI -- 2h

## Exercice 0 : exercice de france-ioi (3pt)

En utilisant une boucle, écrivez un programme python qui calcule et affiche la somme `1³+2³+3³+...+17³`.

??? check "Solution"
	```python
	resultat = 0
	x = 1
	for i in range(17):
		resultat = resultat + x**3
		x = x + 1
	print(resultat)
	```

## Exercice 1 : Programmation en python (7pt)

On veut programmer un jeu de plate-forme à une dimension : un niveau est
composé d'une suite de cases qui peuvent être vides ou
contenir un obstacle de hauteur 1 ou 2, par dessus lesquels le personnage
devra sauter.  
On représente un niveau en python par une liste d'entiers, un par case,
0 si elle est vide et 1 ou 2 selon la hauteur de l'obstacle sinon.
Par exemple le niveau suivant:
```console
  #
_##_#_
```
est représenté par la liste python `[0,1,2,0,1,0]`.

**question 1 (1pt):** Si la variable `niveau` contient une liste, écrivez l'instruction qui affiche le premier élément de cette liste.

??? check "Solution"
	```python
	print(niveau[0])
	```

**question 2 (3 pt):** On exécute le programme suivant:
```python
niveau = [1,0,2,1,0,1]
nombre_obstacles = 0
nombre_cases = len(niveau)
for i in range(nombre_cases):
	if niveau[i] > 0:
		nombre_obstacles = nombre_obstacles + 1
```
Recopiez et complétez le tableau de la valeur des variables au cours de l'exécution.
Ajoutez une ligne par tour de boucle, qui donne les valeurs à la fin du tour de boucle.

|i|niveau[i]|nombre_obstacles|
|---|---|---|
|0|1|1|

??? check "Solution"
	| i | nivau[i] | nombre_obstacles |
	|---|----------|------------------|
	| 0 | 1        | 1                |
	| 1 | 0        | 1                |
	| 2 | 2        | 2                |
	| 3 | 1        | 3                |
	| 4 | 0        | 3                |
	| 5 | 1        | 4                |

**question 3 (2 pt):**\(\*\) Le niveau sera impossible à réussir s'il contient au moins 4
obstacles consécutifs.
Votre voisin a écrit la fonction suivante pour vérifier qu'un niveau est possible:
```python
def est_possible(niveau):
    nombre_cases = len(niveau)
	for i in range(nombre_cases):
		if niveau[i]>0 and niveau[i+1]>0 and niveau[i+2]>0 and niveau[i+3]>0:
			return False
		else:
			return True
```
Il vous montre les tests suivants qui renvoient un résultat juste.

```python
>>>est_possible([1,0,2,2])
True
>>>est_possible([1,1,2,1])
False
```
**a)** Trouvez un test qui renvoie un résultat incorrect, et expliquez pourquoi.

??? check "Solution"
	```python
	>>>est_possible([0,1,1,1,1])
	True
	```
	Le programme renvoie une réponse après avoir testé les 4 premières cases seulement, l'instruction `return` arrêtant tout de suite l'exécution de la fonction.

**b)** Trouvez un test qui va déclencher une erreur de python, et expliquez pourquoi.

??? check "Solution"
	```python
	>>>est_possible([0,0,0])
	```
	Si `niveau` a moins de 4 cases, il y aura une erreur car la fonction essaie de lire en dehors de la liste.


**question 4 (2 pt):**\(\*\) À chaque fois que le niveau contient une case vide suivie
d'une case avec un obstacle de hauteur 2, il faut utiliser un boost. Recopiez et
compléter le code suivant pour que la variable `nombre_boost` contienne le nombre de boosts
nécessaires pour ce niveau. Votre code doit fonctionner quelque soit le niveau
défini à la première ligne.
```python
niveau = [1,0,1,2,0,2]
nombre_cases = len(niveau)
nombre_boost = 0
# votre code ici
```

??? check "Solution"
	```python
	niveau = [1,0,1,2,0,2]
	nombre_cases = len(niveau)
	nombre_boost = 0
	for i in range(nombre_cases-1):
		if niveau[i]==0 and niveau[i+1]==2:
			nombre_boost = nombre_boost + 1
	```

## Exercice 2 : Utilisation de la ligne de commande (7 pt)
**question 1 (1pt par question):**
![](ressources/arborescence.png)
On suppose pour chaque question qu'on est dans le dossier frederic. Écrivez une ou plusieurs commandes à chaque fois pour :

**a)** créer un nouveau dossier `jeux` dans le dossier frederic.  
??? check "Solution"
	```bash
	mkdir jeux
	```
**b)** supprimer le dossier `images`  
??? check "Solution"
	```bash
	rmdir images
	```
**c)** afficher le contenu du fichier fibonacci.py  
??? check "Solution"
	```bash
	cat programmes/fibonacci.py
	# ou bien
	cd programmes
	cat fibonacci.py
	```
**d)** se déplacer dans le dossier `etc`  
??? check "Solution"
	```bash
	cd ../../etc
	# ou bien
	cd ..
	cd ..
	cd etc
	# ou bien
	cd /etc
	```
**e)** copier le fichier `extension.sqh` dans le dossier `programmes`
??? check "Solution"
	```bash
	cp /isn/extension.sqh programmes
	```

_aide : "/" est le nom du dossier tout en haut. Le raccourci ".." est remplacé par
le dossier juste au dessus de celui dans lequel on est. Les commandes utiles ici
sont `cd,cp,cat,mkdir,rmdir`._

**question 2 (2 pt):** Dessinez l'arbre des fichiers et dossiers
créé par ces commandes (en entourant les dossiers, pas les fichiers).
On suppose que le dossier dans lequel on est au départ s'appelle `depart`.
``` bash
mkdir NSI
cd NSI
mkdir projet_web
mkdir python
touch python/script.py
touch README.txt
```

## Exercice 3 : Algorithmique (5 pt, 1 par question)

Suite à une réforme, il n'y a plus de sonneries au lycée l'an prochain et tous les professeurs peuvent choisir librement les horaires de leurs cours (par exemple de 9h42 à 11h14).  
Vous connaissez les horaires de chaque cours, et comme vous adorez apprendre vous voulez trouver le plus grand nombre de cours que vous pouvez suivre. La seule règle est que vous ne pouvez pas suivre deux cours qui ont lieu en même temps.  
On va travailler comme exemple avec la liste de cours suivante :

* Latin: 8h04 -> 9h38
* Histoire: 9h20 -> 10h15
* Maths: 10h02 -> 15h14
* EPS: 12h -> 16h
* Anglais: 15h55 -> 17h

**question 1:** On utilise l'algorithme suivant: on ajoute à chaque fois le cours **qui a la durée la plus courte** parmi les cours compatibles avec ceux qu'on a déjà pris, jusqu'à ce qu'on ne puisse plus en rajouter. Donner les cours qu'on obtient, dans l'ordre où on les a ajoutés.  
??? check "Solution"
	Histoire et Anglais.
**question 2:** Même question en prenant à chaque fois le cours **qui commence le plus tôt**.  
??? check "Solution"
	Latin, Maths, Anglais.
**question 3:** Même question en prenant à chaque fois le cours **qui termine le plus tôt**.  
??? check "Solution"
	Latin, Maths, Anglais.
**question 4:** On se demande si ces algorithmes sont optimaux, c'est à dire si ils donnent toujours le plus grand nombre de cours possibles. Est-ce que les question 1,2,3 permettent de conclure sur certains d'entre eux ?  
??? check "Solution"
	Juste avec cet exemple on ne **peut pas** déduire qu'un des algorithmes est toujours mieux qu'un autre, ni que l'un d'entre eux est optimal (c'est à dire le toujours le meilleur possible). La seule chose qu'on peut déduire c'est que l'algorithme 1 n'est **pas** optimal parce qu'il donne une solution moins bonne que les autres, donc non optimale, sur un exemple. Il ne peut donc pas être le meilleur tout le temps.
**question 5:**\(\*\) Si deux des algorithmes étaient à égalité dans les questions 1,2,3, trouvez une liste de cours où un algorithme est meilleur que l'autre.  
??? check "Solution"
	Si on a:
	
	* Maths: 8h -> 17h
	* Anglais: 9h -> 10h
	* Français: 11h -> 12h
	
	Prendre à chaque fois le cours qui commence le plus tôt donne juste Maths, alors que prendre celui qui finit le plus tôt donne Anglais et Français. On en déduit que la méthode de la question 2 n'est pas optimale.
**question bonus:**\(\*\*\) Est-ce qu'un de ces algorithmes est optimal ? Si oui, essayez de le prouver. Si non, trouvez un contre-exemple.
