# Projet de bibliothèque python

Le but est de construire une petite bibliothèque de manipulation de texte ou d'images, par groupe de 2 à 4.

Le projet est à terminer pour la **3e semaine après les vacances** (17 mai).

??? Info ":new: Aide sur la structure"
    Typiquement, vous aurez dans le dossier de votre projet :
    
    * le fichier de la bibliothèque, par exemple `texte.py` avec dedans le code des fonctions :
    ```python
    def fonction1(param1):
        # blabla
    
    def fonction2(param1, param2):
        # blabla
    ```
    * un ou plusieurs fichiers python de tests, qui vont importer les fonctions de la bibliothèque pour les tester :
    ```python
    from texte import *
    
    assert fonction1(12) == 42
    
    assert fonction2("truc.txt", "valise") == "trucbidule"
    ```
    * éventuellement des fichiers supplémentaires nécessaires aux tests (par exemple, des fichiers textes d'exemple)
    
??? Info ":new: Aide sur la manipulation de texte"
    Pour lire un fichier texte `truc.txt` en python, on peut faire :
    ```python
    f = open("truc.txt")
    texte = f.readlines()
    ```
    La variable `texte` contient alors une liste de lignes, chacune représentées par une chaîne de caractère (avec le
    caractère de retour à la ligne `\n` à la fin).
    
    Si on veut parcourir toutes les lignes avec une boucle for :
    ```python
    for ligne in texte:
        # faire ce qu'on veut avec ligne
    ```
    Ou bien tous les caractères du texte un par un :
    ```python
    for ligne in texte:
        for caractere in ligne:
            # faire ce qu'on veut avec caractere
    ```

??? Info ":new: Aide sur la collaboration et les versions"
    Vous pouvez utiliser le lien nextcloud que je vous ai envoyé sur l'ENT pour vous partager les
    fichiers.
    
    Si vous êtes plusieurs à coder des fonctions indépendantes, vous pouvez les faire dans des fichiers
    différents et les fusionner à la fin. Pensez à vous mettre d'accord sur les noms de fichiers dans ce
    cas pour ne pas avoir des fichiers différents avec le même nom.
    
    De même si vous faites des sauvegardes ou si vous vous envoyer des versions successives de vos fichiers,
    nommez les d'une manière cohérente (par exemple `texte_v1.py`, `texte_v2.py`...).
    
    Pour les tests, vous pouvez toujours les faire dans plusieurs fichiers séparés, ce n'est pas gênant
    (attention là aussi à avoir des noms de fichiers qui seront clairs).
    
    La bonne manière de collaborer ainsi sur un projet de code est d'utiliser un logiciel de gestion
    de version comme [git](https://fr.wikipedia.org/wiki/Git) : un dépôt sur un serveur (github,gitlab
    ou autre) centralise tout l'historique des modifications, chaque personne peut avancer de son côté
    puis envoyer ses modifications au serveur, et il y a une gestion des fusions à faire si plusieurs
    personnes ont modifié le même fichier. Ces logiciels sont un peu techniques à prendre en main juste
    pour le cours de NSI, mais si vous continuez dans l'informatique vous les utiliserez un jour !
    

## Objectifs

S'entraîner à écrire du code propre et documenté :

* en utilisant plusieurs fichiers
* avec des docstrings, des annotations de type
* avec des tests pour chaque fonction

Et de faire des choses intéressantes en manipulation de texte et d'image.

## Consignes

Vous allez devoir créer, dans un même répertoire :

* un fichier python avec le code de votre bibliothèque.
* un ou plusieurs fichiers pour tester et faire la démo de votre code.
* un fichier README.txt qui explique succintement : 
    * la répartition des tâches dans le groupe
    * les aides extérieures éventuelles si utilisées (avec des liens si c'est sur internet)
    * les problèmes rencontrés et les solutions trouvées

Chaque fonction de la bibliothèque doit avoir :

* une docstring d'une à trois lignes (commentaire entre triple guillemets juste sous la ligne du `def`) qui
  explique ce que fait la fonction, ce qu'elle prend en paramètre et ce qu'elle renvoie.
* si possible des annotations de type
* plusieurs tests dans un fichier à part qui vérifie que la fonction renvoie le résultat
  attendu. Il est bien d'écrire ces tests **avant** d'écrire le code de la fonction.
  

## Évaluation

Vous serez évalué sur :

* le sérieux du travail fait en classe et de l'organisation en groupe **4 pts**
* la qualité et le fonctionnement du code **4 pts**
* la qualité de la documentation (docstrings, choix des noms de variable/fonction) et le README **4 pts**
* la qualité des tests **3 pts**
* le nombre et l'intérêt/originalité des fonctions écrites **5 pts**

Les notes pourront être individuelles si la répartition du travail est déséquilibrée.

## Sujet

### Choix 1 : Bibliothèque d'analyse de texte

On représentera un texte comme une liste de chaînes de caractère, chacune des lignes
du texte. Voici quelques idées de fonctions plus ou moins faciles à coder, mais
n'hésitez pas à en inventer d'autres :

* lire(fichier) : renvoie un texte en lisant le contenu du fichier dont on a reçu le nom en paramètre
* nb_lignes(texte)
* nb_mots(texte)
* nb_caracteres(texte)
* mot_le_plus_frequent(texte)
* diff(texte1, texte2) : afficher les lignes différentes entre deux textes
* filtre(texte, motif) : ne renvoie que les lignes qui contiennent le motif
* corrige_majuscules(texte) : rajoute les majuscules au début des phrases si elles sont manquantes.
* print_fautes(texte, erreurs_courantes) : à partir d'un dictionnaire d'erreurs d'orthographe courantes, par
  exemple {"bonjoure":"bonjour"}, afficher les lignes avec les erreurs et les propositions de correction.
* langue(texte) : essaie de deviner dans quelle langue est écrit le texte. Dans sa version la plus simple,
  essayer juste de différencier entre français et anglais en analysant la quantité de certaines lettres.
  On peut ensuite améliorer ça.
* generation_automatique(depart, n) : complète le texte `depart` avec n caractères générés automatiquement,
  en utilisant la fréquence de transition entre les caractères pour écrire un texte "probable".


### Choix 2 : Bibliothèque de manipulation d'image (**un peu plus difficile**)

<!-- lien vers la doc de pillow -->

On utilisera la bibliothèque Pillow pour manipuler les images ([documentation complète](https://pillow.readthedocs.io/en/stable/), 
et [résumé](https://he-arc.github.io/livre-python/pillow/index.html) en français), 
et on fera le choix pour les fonctions de soit représenter les images par :

* directement le type d'image de Pillow, en utilisant les fonctions de Pillow pour lire et écrire les pixels
* ou bien des listes de listes pour avoir un tableau des pixels en 2D (chaque pixel pouvant être un triplet de
nombres si on est en RGB), il faut dans ce cas avoir une fonction de conversion vers et depuis le type de Pillow

Vous pouvez aussi regarder [ce TP de SNT](https://2snt.xyz/images/) pour la partie qui utilise Pillow.

Fonctions possibles:

* lire(fichier) : renvoyer une image à partir du nom d'un fichier d'image
* afficher(image) : utiliser Pillow pour afficher une image
* couleur_majoritaire(image) : renvoie la couleur la plus fréquente dans l'image
* rotation_droite(image)
* changer_luminosite(image, coefficient)
* niveau_de_gris(image) : convertit une image en niveaux de gris
* contours(image) : renvoie une nouvelle image constituée des contours de l'image (là où la couleur d'un pixel à l'autre change brusquement)

* cacher(image, message) : cache un message dans une image
* extraire(image, message) : extrait le message caché par la fonction précédente

* pot_de_peinture(image, x, y, couleur) : applique l'outil "pot de peinture" au pixel (x,y) avec la couleur donnée

### Choix 3 : me demander (**difficile**)

* Vous pouvez essayer de faire une bibliothèque qui gère du son, mais il faut être autonome (je ne pourrai pas
  vous guider beaucoup).
  
* Utiliser la [base de donnée d'altitude](https://geoservices.ign.fr/bdalti) de l'IGN librement disponible pour
  afficher des cartes d'altitudes ou de pente, ou des crêtes, etc (en se limitant à l'Isère par exemple).  
  Ou, plus difficile mais intéressant, fabriquer automatiquement une image qui représente la ligne des montagnes
  visibles en fonction des coordonnées où on est et de la direction dans laquelle on regarde (le test sera de
  vérifier si on obtient la bonne image en regardant le Vercors depuis la fenêtre de la salle de cours). Il faudra
  a priori utiliser du _ray tracing_.

<!-- pareil sur le son, mais je pourrai pas trop aider

ou raytracing avec données ouvertes d'altitudes pour afficher les sommets à un endroit
-> pb de l'altitude de départ rend ça trop galère ?

try to code it to see, or would it spoil the fun ?

-->
