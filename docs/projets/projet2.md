# Petit projet de programmation python

## Fonctionnement

* Par groupes de 2 ou 3 élèves
* Travail en classe 2/3 séances, avec conseils/aide si besoin
* Fin du projet pendant les vacances, en autonomie

Il faudra rendre à la fin, pour chaque groupe :

* le code python
* un document .txt qui explique :
    * le partage des tâches dans le groupe, le mode d'organisation
    * les difficultés rencontrées, les solutions trouvées
    * les **liens** vers les pages utilisées si vous avez trouvé de l'aide sur internet

Il y aura une présentation rapide à l'oral à la classe en janvier (~ 5min) pour:

* montrer le code et faire une démo du programme
* expliquer un ou deux points intéressants pour la classe (un bug rencontré et sa solution, une méthode pour collaborer à plusieurs à distance, etc.)

!!! danger "Avertissement"
    Si vous prenez du code tout fait sur internet, je m'en rendrai probablement compte immédiatement.
	Si vous êtes ensuite incapable d'expliquer le fonctionnement précis du code, vous aurez la note de 0.
	Vous pouvez utiliser internet pour trouver la solution à des **petits problèmes** si **vous comprenez
	la solution réutilisée** et que vous **mettez le lien de la page dans le fichier texte du projet**.

Vous serez évalués sur :

* l'organisation et le sérieux du travail dans les séances en classe
* la qualité du code :
    * découpage en fonctions quand c'est possible
    * noms de variables bien choisis
    * commentaires s'il y a besoin d'expliquer une partie
    * des tests avec **assert** si c'est possible pour valider les fonctions
* le résultat
* la présentation à la classe

## Sujets possibles

Le sujet est ouvert tant que c'est de la programmation python, mais il faut :

* prendre un sujet adapté à ce dont vous êtes capables
* prendre un sujet adapté au temps que vous avez à y consacrer

Il faut d'abord avoir un objectif raisonnable, et vous pourrez ensuite ajouter des
fonctionnalités si vous avez le temps. S'il y a des interactions avec l'utilisateur
on préférera d'abord le faire en mode textuel avec input() et print().

Voici une liste de sujets possibles, avec une indication de la difficulté de \* à \*\*\*.

* **Le jeu du juste prix** \*
  
  Le programme choisit un nombre au hasard, la personne qui joue
  essayer de le trouver et reçoit des indices "plus petit" ou "plus grand". On peut ensuite
  rajouter un score, un nombre d'essais maximum, etc.
  
* **Un jeu de pierre/feuille/ciseaux** \*

  Le programme joue contre l'utilisateur. On peut faire une fonction qui valide le coup
  du joueur, une qui choisit pour l'ordinateur, et afficher le score de chacun.
  Ensuite on peut essayer de faire jouer l'ordinateur plus efficacement (par exemple
  si le joueur joue souvent le même coup, en profiter).
  
* **Le jeu du pendu** \*\*

  Le programme choisit un mot, la personne qui joue essaye de deviner les lettres
  dans le mot et a un nombre d'erreurs maximum avant de perdre.
  On peut commencer avec un mot écrit dans le code, puis utiliser un fichier
  avec une liste de mots et en choisir un au hasard.
  
* **Le jeu du morpion** \*

  Il faut d'abord choisir comment on représente le plateau de jeu.
  Puis on peut séparer le code en plusieurs parties : une fonction
  qui teste si un joueur a gagné, une autre qui vérifie si un coup
  est valide, etc.
  
* **Bataille navale** \*\*

  Sur une grille de taille fixée, le programme joue contre
  un humain. Il faut bien choisir comment représenter les données
  et les coordonnées des cases.
  Le programme peut jouer au hasard d'abord, puis on peut améliorer
  sa stratégie.
  On peut découper le code en plusieurs parties : choisir le placement
  des bateaux, répondre à un coup du joueur (touché/coulé/dans l'eau),
  donner un coup au joueur.
  
* **Participation à Advent of Code** \*\*

  Vous pouvez si ça vous intéresse essayer de faire le plus d'exercices
  d'Advent of Code, et présenter à la fin ceux que vous avez préféré.
  
* **Autre** \*,\*\*,\*\*\*

  Vous pouvez me proposer un autre sujet, je validerai s'il me semble faisable.
