# Prouver la terminaison d'un programme

Un **variant** de boucle est un nombre entier associé à chaque tour d'une boucle **while** qui :

* soit diminue à chaque tour et est minoré (ne descend jamais sous une valeur fixeé, par exemple 0)
* soit augmente à chaque tour et est majoré

Si une boucle possède un tel variant, alors elle se termine (car il n'existe pas de suite infinie
d'entiers croissante et majorée, ou décroissante et minorée).

Trouvez les variants associés aux boucles while des codes suivants:

---

```python
def puissance(x,n):
    """calcule x puissance n (n >= 0)"""
    resultat = 1
    i = 0
    while i < n:
        resultat = resultat * x
        i = i + 1
    return resultat
```

??? check "Solution"
    Un variant de boucle est `i`, qui est:
    
    * un entier
    * majoré par `n` car la boucle ne s'exécute que si `i < n`
    * croissant car à chaque tour on exécute `i=i+1`
    
    donc la boucle termine (car il n'existe pas de suite infinie d'entiers croissants qui soit majorée).

---

```python
def division_euclidienne(a, b):
    """renvoie le quotient et le reste dans la division de a par b (a > 0 et b > 0)"""
    quotient = 0
    reste = a
    while reste >= b:
        reste -= b
        quotient +=1
    return quotient,reste
```

??? check "Solution"
    Un variant de boucle est `reste`, qui est:
    
    * un entier
    * minoré par `b`
    * décroissant car on exécute à chaque tour `reste -= b` (**à condition** que $b > 0$)
    
    donc la boucle termine.

---

```python
def somme_grille(n):
    """additionne toutes les valeurs d'une grille carré (n > 0)"""
    resultat = 0
    i = n-1
    j = n-1
    while i >= 0 and j >= 0:
        resultat = resultat + grille[i][j]
        if i > 0:
            i = i - 1
        else:
            i = n - 1
            j = j - 1
    return resultat
```

??? check "Solution"
    **Ni i ni j ne sont des variants** car il ne sont pas décroissants à chaque tour
    de boucle (selon qu'on exécute la branche du if ou du else).
    
    Par contre on peut prendre comme variant $i+j*n$, qui est un entier décroissant car:
    
    * si on exécute le if, $i+j*n$ décroît de 1
    * si on exécute le else, $i$ augmente de $n-1$ et j diminue de 1 donc $j*n$ diminue de n et $i+j*n$ diminue de 1.
      
    donc la boucle termine bien.

---

```python
def syracuse(n):
    """un peu plus difficile (n > 0)"""
    while n > 1:
        if n % 2 == 0:
            n = n // 2
        else:
            n = 3 * n + 1
```

??? fail "(Non-)solution"
    `n` n'est pas un variant, car selon sa parité il va soit diminuer (divisé par 2) soit
    augmenter (fois 3 plus 1), cela ne permet donc pas de savoir si la boucle va bien se
    terminer.
    
    Savoir si ce programme termine toujours est en fait un problème célèbre non-résolu des mathématiques:
    la [conjecture de Syracuse](https://fr.wikipedia.org/wiki/Conjecture_de_Syracuse)
