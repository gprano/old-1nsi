# Prouver la correction d'un programme avec un invariant

Un **invariant de boucle** est une propriété logique qui est vraie avant le
début d'une boucle, et qui reste vraie après chaque itération (tour) de la boucle.

Une fois qu'on a choisit l'invariant, il faut :

* Montrer qu'il est vrai au début (**initialisation**)
* Montrer que s'il est vrai à un tour de boucle, il reste vrai au tour suivant (**conservation**)

On peut ensuite déduire qu'il est vrai à la fin, ce qui donne (on l'espère) une
propriété utile pour prouver que notre programme est correct.

---

Pour chacune des fonctions suivantes, trouver un invariant et utilisez-le pour prouver que
la fonction renvoie bien le résultat attendu.

---

```python
def puissance(x,n):
    """renvoie x puissance n (n entier >=0)"""
    p = 1
    for i in range(n):
        p = p * x
    return p
```

??? check "Solution"
    Un invariant qui est vrai **à la fin** de chaque tour de boucle est : $p=x^{i+1}$
    
    * **initialisation:** après le premier tour, `p = 1 * x = x` et i vaut 0.
    
    * **conservation:** si à un tour donné on a $p=x^{i+1}$.  
      Au tour suivant on a $i'=i+1$ et $p'=p*x=p^{i+1}=p^{i'+1}$

    Donc la propriété est vraie à chaque tour de la boucle, en particulier au dernier
    tour i vaut $n-1$ donc la valeur renvoyée est $p=x^{n-1+1}=x^n$

---

```python
def somme(l):
    """renvoie la somme des éléments de la liste l"""
    total = 0
    for i in range(len(l)):
        total = total + l[i]
    return total
```

??? check "Solution"
    L'invariant qui est vrai à la fin de chaque tour de boucle est $total = l[0]+...+l[i]$
    
    C'est à dire : total contient la somme des $i+1$ premiers éléments de la liste.

---


```python
def tri_selection(l):
    """trie la liste l en place"""
    n = len(l)
    for i in range(n):
        assert sorted(l[:i]) == l[:i]
        i_minimum = i
        for j in range(i+1,n):
            if l[j] < l[i_minimum]:
                i_minimum = j
        l[i],l[i_minimum] = l[i_minimum],l[i]
```

??? check "Solution"
    L'invariant qui permet de prouver que la liste est bien triée à la fin est:
    
    À la fin du tour de la boucle (extérieure, avec i comme indice), la liste l:
    
    * a ses $i+1$ plus petits éléments placés au début;
    * ces éléments sont placés dans l'ordre;
