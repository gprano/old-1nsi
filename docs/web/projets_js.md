# Mini-projet de jeu sur une grille en javascript

Je vous propose d'essayer de faire votre propre petit jeu ou animation en
javascript basé sur une grille, éventuellement contrôlable avec les touches du
clavier.

Vous pouvez créer un dossier à votre nom sur le lien nextcloud envoyé sur l'ENT
et y mettre vos fichiers, qui seront accessible automatiquement ici : [https://nodfs.xyz/projets_js/](https://nodfs.xyz/projets_js/).

## Éléments d'aide

### Fonction de création d'une grille :

```javascript
function create_grid(nb_lignes, nb_colonnes) {
    let table = document.getElementById("ma_table");
    let tableau = [];
    for(let i = 0; i < nb_lignes; i += 1){
        let tr = document.createElement("tr");
        table.appendChild(tr);
        tableau.push([]);
        for(let j = 0; j < nb_colonnes; j += 1){
            let td = document.createElement("td");
            tr.appendChild(td)
            tableau[i].push(td);
        }
    }
    return tableau
}
```

Si vous avez préalablement créé un élément `<table id="ma_table"></table>` dans
votre code html, cette fonction en fait une grille de `nb_lignes`x`nb_colonnes` et
renvoie un tableau 2D qui contient les objets javascripts de chaque cellule.

Pour créer un tableau de 10x10 et colorier une cellule en rouge, on peut par exemple faire :

```javascript
let tableau = create_grid(10,10)
tableau[3][7].style.backgroundColor = "rgb(255,0,0)"
```

### Capturer les touches du clavier


```javascript
document.addEventListener('keydown', function(event) {
    console.log(event.code) // vous permet de voir les codes des touches dans la console
        
    if(event.code == "ArrowUp"){
        //action pour la flèche du haut
    } else if(event.code == "ArrowDown"){
        // etc
    } 
});

```

### Exécuter une fonction toutes les x millisecondes

```javascript
function f() {
    //blabla
}

setInterval(f, 600); // f sera exécutée toutes les 0.6 secondes
```

### Ajouter une balise HTML via le javascript

```javascript
// on crée un objet js pour un nouvel élément
let paragraphe = document.createElement("p");

// si elem représente la balise <body> par exemple
// cela va ajouter une balise <p> dans la balise body

elem.appendChild(paragraphe)
```

### Générer un nombre aléatoire

On n'a pas directement de `randint` en javascript, juste `Math.random()`
qui renvoie un nombre décimal entre 0 et 1.

On peut faire une fonction qui génère un entier aléatoire comme ceci :

```javascript
function randint(min, max) { // min included, max excluded
  return Math.floor(Math.random() * (max - min) ) + min;
}
```

### Utiliser le clic de la souris sur un élément

On peut initialiser la propriété `onclick` avec une fonction :

```javascript
function gagne() {
    alert("Bravo, tu as trouvé la bonne case !");
    
    //le mot clé this fait référence à l'objet sur lequel on a cliqué
    this.style.backgroundColor = "red";
}

tableau[3][7].onclick = gagne
```
