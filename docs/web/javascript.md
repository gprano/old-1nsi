# Utilisation du langage de programmation javascript

Javascript est un langage de programmation qui a trois caractéristiques importantes :

* c'est un langage de programmation complet (comme python, mais pas comme HTML/CSS);
* il est intégré à HTML/CSS en permettant de manipuler la page web directement;
* tous les navigateurs web (Chrome, Firefox, ...) peuvent l'exécuter;

C'est donc le langage par défaut si on veut ajouter du code exécutable sur une page
web, qui doit s'exécuter côté client c'est à dire sur l'appareil de la personne qui
visite la page.

C'est aussi un langage qui est **interprété** comme python, c'est à dire qu'il n'est
pas compilé en langage machine (comme C ou C++) mais qu'un programme spécialisé lit le
code et l'exécute ligne par ligne. En python, c'est le logiciel python, en javascript
c'est le moteur javascript intégré dans les navigateurs (V8 pour Chrome, Spidermonkey pour Firefox).

Ce TP sera une découverte assez rapide du langage, si vous voulez en apprendre plus
je vous conseille ce site : [https://fr.javascript.info/](https://fr.javascript.info/).

## 1) Découverte de javascript comme langage de programmation

D'abord, comment exécuter du code javascript ? Il suffit d'ouvrir la console
de développement du navigateur avec **F12**, et on a accès dans l'onglet console
à un interpréteur javascript. Pas besoin donc de logiciel spécifique.

On peut écrire le code d'un côté et voir le résultat de l'exécution de l'autre :

![console](console.png)

On va voir que la syntaxe de javascript est assez proche de python, en faisant
un tour rapide des fonctionnalités :

---

### <u>variables</u>

À la différence de python, en javascript on doit **déclarer** une variable
avec `let ma_variable` avant de l'initialiser.

L'initialisation marche ensuite comme en python avec le signe `=` : `ma_variable = 3`.

On peut faire les deux en même temps avec `let ma_variable = 3`.

**Question :** Essayez-le : que se passe-t-il si on re-déclare une variable déjà déclarée ?

**Remarque :** On peut aussi déclarer une variable avec le mot clé `const` au lieu de `let`,
elle ne pourra alors pas être modifiée : `const taille_plateau = 17`. Déclarez
une variable constante et essayez de la modifier pour voir ce qu'il se passe.

!!! info
    La **portée** des variables, c'est à dire la zone du code où elles sont valides quand
    elles ont été déclarées quelque part, est différente en python et en javascript:
    
    * en python une variable est valide dans toute la fonction où elle est déclarée (ou
    dans tout le script si elle est en dehors d'une fonction)
    * en javascript une variable est valide dans le bloc, défini par des accolades, où elle a été déclarée (ou dans
    tout le script hors des blocs)
    
    On verra les blocs en dessous avec if/else/for/while, cela veut dire qu'en javascript une variable déclarée dans un
    if n'existe plus juste après le bloc du if.

---

### <u>valeurs et types</u>

!!! info 
    En javascript il n'y a pas de différence entre entiers et nombres décimaux : les
    deux sont de type `number`. Javascript est très permissif sur ce qu'on peut faire avec
    les valeurs et renvoie rarement une erreur, vous allez le voir.

Calculez dans la console :

* 1 / 2
* 2 \*\* 3
* 1 / 0
* 0 / 0

On remarque qu'on peut tomber sur des valeurs particulières de nombres : `Infinity`, `-Infinity`, `NaN` (not a number).

* Les chaînes de caractères (de type String) fonctionnent presque comme en python, entre simple ou
double guillemets.

!!! info
    Si on veut afficher des variables dans une chaîne de caractères, en python on a le droit aux f-string
    comme ceci : `f"blabla {var} truc"`, et en javascript on peut utiliser les backticks : `` `blabla ${var}` ``.
    
Testez dans la console : 

* "bonjour" + " Andrea"
* "bonjour" + 12

Quelles différence avec python ?

!!! info
    Les booléens fonctionnent comme en python sauf qu'on n'a pas de majuscule à `true` et `false`
    
!!! info
    Il y a deux valeurs spéciales qui jouent le rôle du `None` en python : ce sont `null` et `undefined`.
    `null` sert quand on veut assigner une valeur "vide" à une variable, et `undefined` est ce que contient
    une variable qui a été déclarée mais pas initialisée, ou ce que renvoie une fonction sans return.

* La plupart des opérateurs de calcul et de comparaison fonctionnent comme en python.

* L'opérateur **and** de python s'écrit **&&**, **or** devient **||** et **not** devient **!**.

---

### <u>print/input</u>

On peut remplacer le print de python par la fonction alert() qui affiche un pop-up,
ou par la fonction console.log() qui affiche le message dans la console de développement.

On peut utiliser `x = prompt("donne une valeur pour x")` pour demander une valeur comme
avec le input de python.

**Exercice** : écrivez et exécutez du javascript qui demande une année de naissance avec prompt(),
stocke ce résultat dans une variable et affiche avec alert() un message qui donne l'âge correspondant en 2022.

<!-- confirm pour demander un booleen -->

---

### <u>if/else/for/while</u>

* En python, un bloc de code est créé par une instruction qui termine par les deux points
et délimitée parce qu'elle est indentée à droite. En javascript comme dans beaucoup de langages,
on utilise les accolades pour ça : 

```javascript
if (...) { 
    ... 
    ...
}
else {
    ...
    ...
}

while (...) {
    ...
    ...
}

for (...) {
    ...
    ...
}
```

Les if/else/while fonctionnent comme en python sauf que la condition est entre parenthèses.

**exercice** : déclarez une variable a, assignez-lui la valeur 10, et faites une boucle while
qui continue tant que a est supérieur ou égal à 0, qui descend a de 1 à chaque tour et affiche
sa valeur dans la console avec console.log().

Le for est un peu différent, par exemple :
```javascript
for (let i = 0; i < 10; i += 1) {
    alert(i)
}
```
Les parenthèses contiennent trois instructions : une qui s'exécute au début, une qui donne la condition
pour que la boucle continue, et une qui donne l'instruction exécutée à chaque nouveau tour de boucle.

Cela ressemble plus à une boucle while améliorée qu'au for de python, mais il existe aussi des manières
de faire un for comme en python (en parcourant les valeurs d'une liste/dictionnaire/etc).

---

### fonctions

Comme en python, avec la syntaxe :

```javascript

function ma_fonction(param1, param2) {
    message = "J'ai été appelé avec les paramètres " + param1 + " et " + param2
    
    return message
}
```

**exercice :** définissez une fonction `triple_ou_divise` qui prend un paramètre
`n` et renvoie n divisé par 2 si n est pair, et 3*n+1 sinon. Testez-la.



---
### listes/dictionnaires

Les **array** de javascript ressemblent aux listes python : 

```javascript

let arr1 = [] //tableau vide, comme la liste vide

let arr2 = [12, 3, 42]

alert(arr2[0]) //à tester

arr1.push(10) //push au lieu de append
arr1.push(12)

alert(arr1)
```

Les **objets** de javascripts ressemblent un peu aux dictionnaires python : 

```javascript

let user1 = {} // objet vide

let user2 = { name: "Abdel", age: 17 } // objet avec deux propriétés et les valeurs associées

console.log(user2["name"]) // comme en python
console.log(user2.name) // autre manière équivalente plus utilisée en javascript, mais ne marche pas avec des espaces ou tirets

for (key in user2) { // boucle for similaire à python pour parcourir les dictionnaires
    console.log(key)
    console.log(user2[key])
}
```

**exercice :** `document` est un objet qui sert à manipuler la page web sur laquelle on se trouve.
Faites une boucle for qui affiche dans la console toutes les propriétés de l'objet document.

## 2) Retour sur HTML/CSS et exemple

On va partir sur ces fichiers d'exemple, copiez-les et enregistrez-les dans un dossier créé pour l'occasion.
Vous pourrez ensuite les éditer avec `vscode`.

Fichier html:
```html
<!DOCTYPE html>
<html>

<head>

<title>titre de ma page</title>
<link rel="stylesheet" href="style.css">
</head>

<body>

<table>
    <tr>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
    </tr>
</table>

</body>
```
Fichier css:
```css
td {
    width : 30px;
    height : 30px;
    border : 1px solid black;
}

```

**exercice :** Mettez la couleur d'arrière-plan (propriété css backgrund-color) de body
à la couleur que vous voulez. Et rajoutez une ligne dans la grille pour qu'elle soit de 3 par 3.

## 3) Quelques fonctionnalités de manipulation de la page web en javascript

### Récupérer les objets de balises

!!! info
    Pour des raisons de sécurité, le code javascript exécuté sur une page ne peut
    pas accéder aux informations des pages ouvertes dans les autres onglets, ni
    aux fichiers de l'ordinateur. Il peut communiquer sur le réseau avec le serveur
    qui a envoyé la page, mais pas avec d'autres serveurs sauf autorisation explicite.

En partant de l'objet `document` qu'on a déjà vu, on peut récupérer des objets
associés à chaque balise html de la page. Voici deux manières de faire :

**Avec l'attribut id et getElementById:**
```html

<p id="paragraphe1"> Ce texte est dans une balise identifiée de manière unique </p>

<script>
elem = document.getElementById("paragraphe1")

//remarque : si on a pas redéfini de variable avec le même nom, on peut utiliser directement l'id
//comme variable qui contient l'objet :

console.log(paragraphe1.innerHTML)

</script>
```

**Avec querySelectorAll en mettant n'importe quel sélecteur CSS**

```javascript
let tous_h2 = document.querySelectorAll("h2") // sélectionne dans une collection tous les objets des balises h1

for (let elem of tous_h2) { // ATTENTION on utilise of et pas in, car on ne veut pas les propriétés
    alert(elem.innerHTML)
}

// on peut aussi accéder aux éléments avec : tous_h2[0], tous_h2[1], ...
// et le nombre d'éléments est dans la propriété tous_h2.length
```
Testez ce script, qui affiche le contenu de tous les titres h2 de la page.

### Modifiez les objets de balise

Deux manières basiques de modifier directement une balise `elem` :

* le contenu de la balise est dans la propriété `elem.innerHTML`, qu'on peut modifier.

* le style de la balise peut être modifié avec `elem.style` (il y a des sous-propriétés pour chaque propriété CSS, utilisez l'auto-complétion).

**Exercice :** dans la console javascript, récupérer l'objet de la balise body dans une variable, et changez la couleur d'arrière plan de la page.

**Exercice :** changez tout le contenu de la page (de la balise body) en "oups, j'ai tout effacé!"

**Astuce :** il y a une propriété booléenne `hidden` qui permet facilement de cacher/afficher un élément.

### Lier du code 

Fonction appelée lorsqu'on clique sur un bouton : (possible aussi si on passe juste la souris dessus)
```html
<button id="button">Alt+Shift+Click on me!</button>

<script>
  button.onclick = function(event) {
    alert("on a cliqué sur le bouton !")
  }
</script>
```

Оn peut aussi intercepter les actions du clavier :

```javascript
document.addEventListener('keydown', function(event) {
  if (event.code == 'KeyZ' && (event.ctrlKey || event.metaKey)) {
    alert('Undo!')
  }
  //remarque : on peut utiliser event.code ou event.key qui prennent des valeurs un peu différentes
  //par exemple ici even.code est KeyZ et event.key est Z
});
```

**Exercices** : Rajoutez une balise script à la fin du code HTML d'exemple, et essayez d'ajouter des fonctionnalités, par exemple :

* changer la couleur d'arrière plan de la page si on appuie sur une touche spéciale.
* mettre des carrés de la grille d'une autre couleur avec d'autres touches, et les faire redevenir blanches si on clique dessus.
* etc, à vous de jouer.


### Complément : l'arbre DOM

On avait vu que la structure HTML d'une page était celle d'un arbre, qu'on appelle le "Document Objet Model" (DOM).

On utilise beaucoup cette structure d'arbre en javascript pour parcourir le contenu html et passer d'une balise à l'autre.

Vous pouvez lire ces deux pages pour en savoir plus :

* [https://fr.javascript.info/dom-nodes](https://fr.javascript.info/dom-nodes)
* [https://fr.javascript.info/dom-navigation](https://fr.javascript.info/dom-navigation)

