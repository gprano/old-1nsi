# Partie 1 : Portes Logiques et Tuyauterie

L'objectif est de construire successivement les puces suivantes.

Pour chacune, on a le droit d'utiliser les puces déjà construites avant.

Il faut à chaque fois dessiner un circuit en les connectant.
Par exemple celui-ci qui a deux entrées a et b et une sortie out:

![Nimp](Nimp.svg)

## Portes logiques

Ces puces permettent de réaliser des opérations logiques élémentaires.

### Nand

* Entrée : a,b
* Sortie : nand(a,b) vaut 0 si et seulement si a et b valent 1

* table de vérité :

| a | b | Nand(a,b) |
|---|---|---|
|0|0|1|
|0|1|1|
|1|0|1|
|1|1|0|

Ce composant est **donné** : il n'y a rien à faire.

### Not
_Le NON logique_

* Entrée : in
* Sortie : out est l'inverse de in

* table de vérité :

| in | Not(in) |
|---|---|
|0|1|
|1|0|

??? indice
	Il n'y a besoin que d'une seule porte Nand.

### And
_Le ET logique_

* Entrée : a,b
* Sortie : and(a,b) vaut 1 si et seulement si a et b valent 1

* table de vérité :

| a | b | Nand(a,b) |
|---|---|---|
|0|0|0|
|0|1|0|
|1|0|0|
|1|1|1|

??? indice
	Utilise Nand et Not.

### Or
_Le OU logique_

* Entrée : a,b
* Sortie : out(a,b) vaut 1 sauf si a et b valent 0

* table de vérité :

| a | b | Or(a,b) |
|---|---|---|
|0|0|0|
|0|1|1|
|1|0|1|
|1|1|1|

??? Indice 1
	utilise : Not, Nand
??? Indice 2
	Penser négatif, c'est quoi le contraire de "a ou b"
??? Indice 3
	Le contraire c'est non-a et non-b.
??? Indice 4
	Utilise : 2 Not, 1 Nand (ou 3 Not, 1 And)
<!--* solution : or(a,b) = nand(not(a),not(b))-->

### Xor
_Le OU exclusif : comme dans "fromage <u>ou</u> dessert"_

* Entrée : a,b
* Sortie : xor(a,b) vaut 1 si exactement une des entrées vaut 1

Table de vérité :

| a | b | Xor(a,b) |
|---|---|---|
|0|0|0|
|0|1|1|
|1|0|1|
|1|1|0|


??? "Indice 1"
	Utiliser la table de vérité (les lignes qui sont à 1)
	
??? "Indice 2"
	On peut utiliser 2 And, 2 Not, 1 Or  
	Ou bien 1 Or, 1 And, 1 Nand
	
<!--
* intermédiaire
* tip : utiliser la table de vérité
* solution1: xor(a,b) = and(a,not(b)) or and(not(a),b)
* solution2:          = or(a,b) and nand(a,b)-->


### And16, Or16, Not16
_Les versions à 16 fils de And,Or,Not_

* Entrée : a[16],b[16] (16 fils) (sauf pour Not il y a juste a)
* Sortie : out[16] (16 fils)

La sortie est l'application de And,Or ou Not sur
chaque fil. Par exemple out[7] = And(a[7],b[7]).
C'est ce qu'on appelle les opérateurs bit-à-bit.

Le circuit est répétitif et facile à construire.

### Or8Way
_Un Or sur 8 fils_

* Entrée : in[8]
* Sortie : out vaut 1 si au moins un des fils d'entrée vaut 1.


??? "Indice 1"
	Utilise seulement la porte Or.
	
??? "Indice 2"
	Le circuit ressemblera à un tableau de tournoi.
	
	
## Tuyauterie

Toutes ces puces permettent de faire des sortes d'interrupteurs : en fonction
de la valeur sur un ou plusieurs fils, on va "brancher" des fils d'un côté ou d'un autre.

Pour les **multiplexeurs** on a plusieurs fils en entrée, et en fonction de la valeur
de fils **sélecteurs** on va choisir lequel on branche sur la sortie.

Pour les **démultiplexeurs** c'est l'inverse : on a un fil en entrée et selon les sélecteurs
on va choisir sur lequel des fils en sortie on va le brancher (les autres on mettra 0).

### Mux
_Le multiplexeur : choisit a ou b pour la sortie selon la valeur du sélecteur_

* Entrée : a,b,sel
* Sortie : mux(a,b,sel) vaut la même chose que a si sel vaut 0, sinon la même chose que b

??? "Indice 1"
	utilise And,Or,Not
??? "Indice 2"
	faire une séparation entre les deux cas possibles selon que sel vaut 0 ou 1.
<!--
* intermédiaire
* utilise And,Or,Not
* faire une disjonction des deux cas possibles selon que sel vaut 1 ou 0
* solution : and(a, not(sel)) or and(b,sel)-->

### Mux16
_La version 16-bits de Mux_

* Entrée : a[16], b[16], sel
* Sortie : out[16] tel que out vaut a si sel vaut 0, sinon out vaut b

??? "Indice"
	Utilise Mux 16 fois, répétitif.
<!--
* "facile", répétitif
* utilise Mux 16 fois-->

### Mux4Way16
_Multiplexeur 16 bits à 4 entrées (et donc sélecteur sur 2 bits -> 4 possibilités)_

* Entrée : a[16],b[16],c[16],d[16], sel[2]
* Sortie : out[16] vaut:
  * a si sel vaut 00
  * b si sel vaut 01
  * c si sel vaut 10
  * d si sel vaut 11
 
??? "Indice"
	utilise Mux16 (3 fois)
 
### Mux8Way16
_Multiplexeur 16 bits à 8 entrées (sélecteur sur 3 bits -> 8 possibilités)_

* Entrée : a[16],b[16]...h[16]
* Sortie : out[16] vaut :
  * a si sel vaut 000
  * b si sel vaut 001
  * ...
  * h si sel vaut 111

??? "Indice"
	utilise Mux4Way16 (2 fois) et Mux16 (1 fois)

### DMux
_Démultiplexeur : opération inverse d'un multiplexeur, une seule entrée est envoyée sur une des deux sorties selon le
sélecteur, et l'autre vaut 0_

* Entrée : in,sel
* Sortie : a,b avec a=in,b=0 si sel vaut 0, et a=0,b=in si sel vaut 1

??? "Indice"
	Faire séparément a et b
??? "Indice"
	Utilise Not,And

<!-- * intermédiaire (conceptuellement difficile ?)
* utilise Not, And
* Faire séparément a et b
* solution : a = and(not(sel),a)
	         b = and(sel,b) -->
			 
### DMux4Way
_Version à 4 sortie du Démultiplexeur, avec un sélecteur sur 2 bits_

* Entrée : in,sel[2]
* Sortie : a,b,c,d qui valent :
  * in,0,0,0 si sel vaut 00
  * 0,in,0,0 si sel vaut 01
  * 0,0,in,0 si sel vaut 10
  * 0,0,0,in si sel vaut 11

??? "Indice"
	Utilise DMux et c'est tout

<!--
* intermédiaire/difficile ?
* utilise : DMux-->

### DMux8Way
_Démultiplexeur à 8 sorties, avec un sélecteur sur 3 bits_

* Entrée : in,sel[3]
* Sortie : a,b,c,d,e,f,g,h qui valent :
  * in, 0, 0, 0, 0, 0, 0, 0 si sel vaut 000
  * 0, in, 0, 0, 0, 0, 0, 0 si sel vaut 001
  * ...
  * 0, 0, 0, 0, 0, 0, 0, in si sel vaut 111

??? "Indice"
	utilise DMux, DMux4way
