# Construire un ordinateur avec Nand

> Quelque chose de tellement simple. Et ce qu'on construit avec est tellement complexe, que ça nous dépasse.  
> Presque.
> — John Ashbery (traduction)

L'objectif de ce projet est de "construire un ordinateur" à partir de "presque rien".

Comme ce n'est pas un cours d'électronique, on ne va pas physiquement construire l'ordinateur.
On va supposer qu'on nous a donné des composants électroniques vraiment très simples : des portes Nand.

### Qu'est-ce qu'une porte Nand ?

C'est un composant électronique qui a deux fils électriques en entrée, et un fil en sortie.

Sur chaque fil il y peut avoir soit une tension haute, par exemple 5V, qui servira à représenter
la valeur **1**, ou une tension basse, souvent 0V, qui servira à représenter la valeur **0**.

Que fait la porte Nand ? Elle met sur le fil de sortie :

* 0 si les deux fils d'entrée sont à 1
* 1 sinon

On la représente comme ceci :

![porte nand](nandgate.png)

On peut représenter l'action de la porte Nand par une **table de vérité** :
un tableau qui donne pour chaque combinaison des valeurs en entrée la
valeur en sortie.

| a | b | Nand(a,b) |
|---|---|---|
|0|0|1|
|0|1|1|
|1|0|1|
|1|1|0|

### Qu'est-ce qu'on va faire avec ça ?

On suppose maintenant qu'on a une réserve illimitée de portes Nand, et on veut construire un ordinateur. Pour ça on va pouvoir brancher les portes Nand les unes avec les autres comme des legos.

Si on essaye de faire un ordinateur d'un coup, avec des milliers de portes Nand, on ne va jamais y arriver.

L'idée va être de construire des petits circuits (ou puces) avec les portes Nand qui auront des fonctions précises, et qu'on pourra réutiliser ensuite. Ainsi, en partant du bas et en construisant des puces toujours plus sophistiquées, on va finir par arriver à un ordinateur !

Il y aura quelques exceptions à la règle "utiliser seulement la porte Nand" qui seront expliquées au fur et à mesure.

---

Ce projet est une adaptation de la première partie du cours [Nand2Tetris](https://www.nand2tetris.org/course).

