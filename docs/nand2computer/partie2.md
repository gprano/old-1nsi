# Partie 2 : Unité Arithmético-Logique

### HalfAdder (demi-additionneur)

* Entrée : a,b
* Sortie : sum et carry, la somme et la retenue de a+b

Un Half-Adder fait une "colonne" de l'addition posée en ajoutant
deux chiffres binaires.

??? indice
	Écrire la table de vérité et la comparer à celle de Xor et de And.
	
### FullAdder (additionneur)

* Entrée : a,b,c
* Sortie : sum et carry, la somme et la retenue de a+b+c

Quand on pose une addition, si on a une retenue pour une colonne il faut
alors additionner *trois* chiffres binaires, c'est ce que fait le
FullAdder.

??? indice
	On peut utiliser deux HalfAdder et un Or.
	
### Add16

* Entrée : a[16], b[16]
* Sortie : out[16]

Additionne deux nombres sur 16 bits, en utilisant la méthode du primaire
quand on pose une addition, grâce aux FullAdder. Bien propager les retenues.

La dernière retenue n'est pas prise en compte, c'est à dire que si le résultat
dépasse 16 bits, ce qui dépasse est ignoré.

### Inc16

* Entrée : in[16]
* Sortie : out[16]

Ajoute 1 au nombre en entrée.

??? indice
	Application directe de Add16. On a le droit de prendre des fils d'entrée qui
	valent 0 ou 1 de manière constante.
	
## ALU

L'unité arithmético-logique est la partie calculatoire du processeur.

Étant donné deux nombres x et y de 16 bits en entrée et une commande qui indique quelle 
opération on veut faire, l'ALU doit renvoyer le résultat de cette opération en sortie.

C'est ici qu'on a une grosse partie de design, il y a plusieurs façons de faire :

* Les processeurs à jeu d'instruction réduit, dits [RISC](https://fr.wikipedia.org/wiki/Processeur_%C3%A0_jeu_d%27instructions_r%C3%A9duit), ne permettent que les calculs de base. Le reste sera programmé à partir des calculs de base par les logiciels. La plupart des architectures modernes comme les architectures ARM de vos téléphones sont de la famille RISC.

* Les processeurs à jeu d'instruction étendu, dits [CISC](https://fr.wikipedia.org/wiki/Microprocesseur_%C3%A0_jeu_d%27instruction_%C3%A9tendu), permettent plus de calculs, au prix de circuits plus volumineux. Ce type est plus ancien, mais l'architecture x86 d'Intel en fait partie et c'est elle qui est utilisée sur la plupart des ordinateurs de bureaux actuels.

On va ici se limiter à peu de calculs, la multiplication par exemple ne sera pas supportée par notre processeur et il faudra la programmer avec des additions.

**Comment faire le circuit d'une ALU ?**

Le design _naïf_ serait de faire en parallèle les circuits pour chacun des *n* résultat qu'on veut pouvoir calculer (x+y, x+1, x-1, y+1, y-1, 1, -1, 0, x and y, x or y...), puis d'avoir un Mux*n*Way16 qui prend ces n résultats et la commande comme sélecteur pour choisir lequel on met en sortie.

En fait on peut faire beaucoup plus efficace avec quelques astuces de calcul binaire qui permettent d'obtenir les résultats voulus, voici comment notre ALU va fonctionner :

* En entrée : 
    * x[16], y[16] les nombres de 16-bits sur lesquels on va calculer
	* zx s'il vaut 1, on remplace x par zéro
	* nx s'il vaut 1, on remplace x par sa négation binaire
	* zy,ny même rôle pour y
	* f : on calcule x+y si f=1 et x And16 y si f=0
	* no s'il vaut 1, on remplace le résultat par sa négation binaire
* En sortie :
    * out[16] le résultat sur 16 bits
	* Plus deux informations sur le résultat qui serviront ensuite :
        * zr vaut 1 si le résultat vaut zéro
		* ng vaut 1 si le résultat est négatif

??? "Conseils pour réaliser le circuit de l'ALU"
	Revoir la porte Mux16 qui sera beaucoup utilisée.  
	Puis gérer progressivement :
    
    * zx, en créant (sur 16 fils) x2 qui vaut x
		  si zx vaut 0 et 0000000000000000 si zx vaut 1.
	* nx, qui reprend x2 et le remplace par sa négation 
		  si nx vaut 1, en mettant le résultat sur x3.
	* idem sur zy,ny
	* calculer x3+y3 et x3$\land$y3
	* choisir l'un ou l'autre en fonction de f
	* faire la négation du résultat si no vaut 1
	* trouver un circuit pour zr à partir du résultat
	* trouver un circuit pour ng à partir du résultat
	
### Comprendre les calculs de l'ALU

L'ALU telle qu'on l'a faite est un circuit électronique, mais on peut écrire la fonction
suivante en python qui fait le même calcul :

```python
def alu(x, y, zx, nx, zy, ny, f, no):
    if zx == 1:
        x = 0
    if nx == 1:
        x = ~x # ~ est le NOT bit-à-bit en python
    if zy == 1:
        y = 0
    if ny == 1:
        y = ~y

	if f == 1:
        resultat = x + y
    else:
        resultat = x & y # & est le AND bit-à-bit en python

	if no == 1:
        resultat = ~resultat

    return resultat
```

**Travail à faire :** Copiez/collez ce code dans Thonny (installable facilement si vous ne l'avez pas déjà),
et appelez la fonction alu sur des exemples comme `alu(100,20,1,1,0,0,1,0)`.
Le but est de retrouver expérimentalement quelles valeurs de zx,nx,zy,ny,f,no il faut choisir pour faire les calculs
suivants:

* **facile:** x + y, x & y, x, y, 0, -1
* **moyen:** x - 1, y - 1, -2, 1
* **un peu compliqué:** x + 1, x | y (le OR bit à bit), x - y

Complétez ce tableau en rajoutant une ligne par calcul:

| zx | nx | zy | ny | f | no | calcul obtenu ? |
|---|---|---|---|---|---|---|
|0|0|0|0|1|0|x+y|

Essayez ensuite de prouver ces résultats en utilisant:

* la formule qui relie la négation binaire aux nombres négatifs : **non x = -x - 1**.
* la formule qui relie le AND, le NOT et le OR : **non(non A et non B) = A ou B**.
