
# Année 2022-2023

...

# Entraînement ou remise à niveau pour la Terminale NSI

Faire au moins quelques exercices du premier lien de temps en temps pour éviter de trop rouiller ✨

Si vous aviez beaucoup de difficultés en programmation cette année et que
vous voulez avoir vos chances en Terminale, il serait bien d'en faire le plus possible avant
la rentrée 🔨

## :new: Cours interactif de python sur [https://fr.futurecoder.io/](https://fr.futurecoder.io/)

C'est nouveau, ça vient d'être traduit, et ça a l'air très bien : vivement conseillé.

## Exercices pratiques sur [e-nsi.gitlab.io/pratique/](https://e-nsi.gitlab.io/pratique/)

Exercices dans le style de ceux de l'épreuve pratique du bac, que vous pouvez faire dans le
navigateur sans rien installer.

**Très recommandé**, en particulier ceux de la section "À maîtriser".

## Cours et problèmes de [www.france-ioi.org/](https://www.france-ioi.org/)

Vous devriez refaire les deux premiers niveaux si vous ne les maîtrisez pas totalement.

Les niveaux suivants sont très bien pour acquérir un (très) bon niveau en programmation et algorithmique.

## Un [MOOC sur python](https://www.fun-mooc.fr/fr/cours/python-3-des-fondamentaux-aux-concepts-avances-du-langage/)

Vous pouvez essayez si vous préférez ce format, en le suivant bien dans l'ordre. Ce cours va
bien plus loin que ce qu'on fait en NSI, il n'est donc pas nécessaire de le faire en entier.

## Autres cours ou sites d'entraînement

* Relire la section "Python" de ce site

* Retourner sur Capytale revoir ou terminer les activités qu'on a faites cette année

* D'autres exercices python sur [https://www.hackinscience.org/exercises/](https://www.hackinscience.org/exercises/)

* Un [autre site](https://diraison.github.io/Pyvert/) de mini-exercices dans le navigateur, classés par niveau et thèmes.

* Un [site](https://glassus.github.io/terminale_nsi/T6_6_Epreuve_pratique/BNS_2022/) qui présente les exercices de la
  banque de sujets de l'épreuve pratique 2022, avec les solutions.
  
* Des sites de compétition/jeux de programmation :
    * [Codeforces](https://codeforces.com) : compétitions en temps réel régulièrement, en anglais
    * [Prologin](https://prologin.org/) : anciens sujets du concours disponibles pour s'entraîner
    * [CodinGame](https://www.codingame.com/)
