# Liste et fonction en python

```python
def appartient(v, t):
	for i in range(len(t)):
		if t[i] == v:
			trouvee = True
		else:
			trouvee = False
	return trouvee
```

On prétend que cette fonction teste l'appartenance de la valeur v au tableau t (t est une liste python).

Donner des tests qui montrent que cette fonction est incorrecte.

# Un peu de ligne de commande

```bash
mkdir dossierA
cd dossierA
mkdir dossierB
mkdir dossierC
cd dossierC
touch fichier1.txt
cd ..
touch fichier2.txt
cd ..
mkdir dossierD
```

Faites un plan (arborescence) des fichiers et dossiers créés par ces commandes.
