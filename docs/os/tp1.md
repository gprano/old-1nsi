# Utilisation de la ligne de commande

## Mise en place

On va utiliser le site internet [Katacoda](https://www.katacoda.com/courses/ubuntu/playground), qui crée temporairement une machine virtuelle linux sur un serveur et nous y donne accès par une ligne de commande (vous pouvez mettre n'importe quoi comme email/mot de passe et cliquer sur "sign up", on n'en aura pas besoin après).

Vous êtes maintenant connecté avec les pleins pouvoirs sur cette machine. Toutes les commandes que vous avez vu en jouant à Terminus sont valables, voici un rappel des plus courantes :

| Commande (en majuscule, les paramètres à modifier) | Origine du nom | Explication |
|---|---|---|
| ls | **l**i**s**t |liste les fichiers et dossiers présents dans le répertoire actuel (on peut ajouter l'option -a pour voir les fichiers cachés, et l'option -l pour avoir plus d'informations) |
| pwd | **p**resent **w**orking **d**irectory |donne le répertoire actuel |
| cat FICHIER || affiche le contenu de FICHIER|
| cp FICHIER RÉPERTOIRE | **c**o**p**y| copie FICHIER dans RÉPERTOIRE|
| mv FICHIER RÉPERTOIRE | **m**o**v**e| déplace FICHIER dans RÉPERTOIRE|
| mv FICHIER AUTRE\_NOM | | renomme FICHIER en AUTRE\_NOM |
| man COMMANDE | **man**uel | affiche l'aide de COMMANDE (**q** pour quitter)|
| mkdir RÉPERTOIRE | **m**a**k**e **dir**ectory | crée le répertoire RÉPERTOIRE |
| rm FICHIER | **r**e**m**ove | supprimer FICHIER |
| rmdir RÉPERTOIRE | **r**e**m**ove **dir**ectory | supprimer RÉPERTOIRE (s'il est vide) |


## Un peu de réseau

### ping

Pour savoir si vous arrivez à contacter une machine, la commande **ping** envoie des messages et attend de recevoir des accusés de réception. Essayez :

```bash
ping ADRESSE
```

!!! info 
	ADRESSE doit être une adresse IP ou un nom de domaine. 
	
	La commande ne s'arrête pas toute seule, il faut forcer l'arrêt avec **Contrôle-C**.

Quelles informations obtenez-vous avec les réponses ?

??? hint "Réponse"
	On obtient le temps en millisecondes pris par le message pour faire l'aller/retour

### wget

Pour récupérer un fichier sur internet, on peut utiliser la commande **wget**.

Exécutez les commandes suivantes :

```bash
wget nodfs.xyz/programmes.zip
```

Vérifiez avec `ls` que le fichier a été téléchargé. C'est une archive zip, on peut l'extraire avec :

```bash
unzip programmes.zip
```

Vous pouvez refaire `ls` puis utiliser `cat` si vous voulez voir les fichiers qu'on a obtenu.

## Exécuter un programme python

!!! info
	Python est un langage **interprété**, ça veut dire que pour exécuter un programme, on a toujours besoin du logiciel python qui va lire et exécuter le code ligne par ligne. Pour exécuter le fichier `programme.py` par exemple, on va entrer :

```sh
python3 programme.py
```

On utilise `python3` parce que sur certains systèmes `python` appelle l'ancienne version de python.

Lisez le fichier `programme.py` avec `cat`, et exécuter-le. Que fait ce programme ?

??? hint "Réponse"
	Il affiche pour i allant de 0 à 14 la valeur de i et de 10 puissance i.

## Exécuter un programme en C

Lisez le fichier programme.c, qui contient un code qui fait la même chose mais dans le langage de programmation C. Qu'est-ce qui est différent entre les deux codes ? Qu'est-ce qui est commun ?

??? hint "Réponse"
	Parmi les différences entre C et python :
	
	- Le type des variables doit être indiqué explicitement : `int x = 0;`.
	- Les blocs sont marqués par des accolades {} au lieu de l'indentation (décalage).
   

Ce langage n'est pas interprété mais **compilé**, il faut donc deux étapes pour l'exécuter :

1. On utilise le compilateur gcc pour produire un fichier exécutable à partir du code :
  ```bash
  gcc programme.c -o programme
  ```
2. On exécute le fichier exécutable qui a été créé :
  ```bash
  ./programme
  ```

Obtient-on le même résultat qu'avec le programme python ?
  
??? hint "Réponse et Explication"
	Non, à partir de 10^10 les valeurs des puissances de 10 sont fausses.
	
	!!! info
		En fait, en python on a de la chance car les entiers peuvent être aussi grands qu'on veut. Dans la plupart des langages comme C, les entiers sont stockés avec un nombre de bits fixé (32 ou 64 en général). Comme 10^10 est plus grande que 2^32, on a un dépassement et le résultat est faux.
	
	!!! bug ""
		Dans un langage comme python, on s'attendrait à avoir une erreur pour nous informer. En C, il n'y a pas vraiment de vérification pendant l'exécution (il y en a certaines à la compilation), et donc certains bugs peuvent passer inaperçus. Ce type de bug (_integer overflow_ en anglais) est courant. En 1996, une fusée Ariane 5 s'est écrasée à cause d'un dépassement de la taille des entiers !
	
	Cette absence de vérification permet néanmoins de gagner du temps (C est un langage plus rapide que python).
  
Vous pouvez aussi regarder à quoi ressemble le fichier exécutable avec `cat`.

??? hint "Réponse"
	C'est illisible, parce que ce n'est pas un fichier avec du texte mais des instructions pour la machine directement en binaire !

!!! info "Le saviez-vous ?"
	Le programme python lui-même, c'est à dire l'interpréteur qui permet d'exécuter du code python, est écrit... en C !
	
	S'il était écrit en python, on aurait un problème : il aurait besoin de l'interpréteur python pour s'exécuter, c'est à dire de lui-même, qui aurait donc encore besoin de lui-même... on tourne en rond.
	


## Mesurer la vitesse d'exécution

Que fait le code python contenu dans le fichier `compteur.py` ?

??? hint "Réponse"
	Il augmente de 1 la valeur d'une variable un million de fois, puis l'affiche.

Lisez le fichier `compteur.c`, qui fait la même chose en C.

Pour mesurer le temps d'exécution d'une commande, on peut utiliser :

```bash
time COMMANDE
```

Mesurez le temps d'exécution du programme python, puis compilez et mesurer le temps
d'exécution du programme C. Combien de fois plus rapide est la version en C ?

??? hint "Réponse"
	```bash
	time python3 compteur.py
	gcc compteur.c -o compteur
	time ./compteur
	```
	Ça peut varier selon les systèmes, mais de l'ordre de 20x plus rapide en C pour ce programme.

