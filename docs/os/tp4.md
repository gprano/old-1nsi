# Utilisation avancée de la ligne de commande (redirections et compositions de commandes)

_cette partie n'est pas au programme et n'est pas à connaître, mais tout ça est
très utile si vous voulez devenir meilleur-e (et si vous continuez à faire de l'informatique
et de la programmation, vous aurez besoin de la ligne de commande linux à un moment !)_

<!--

redirection avec des pipes

redirection avec > et < et >>, sur des fichiers

entrée standard, sortie standard, erreur standard (stdin,stdout,stderr)

les fichiers particuliers /dev/null, /dev/random

les variables d'environnement (en particulier PATH, HOME, LANG...)

écrire des programmes en bash vs scripter en python
(on peut faire les deux, dépend des préférences et usages, petit exemple)

exemples intéressants :
ls | rev | cut -s -d'.' -f1 | rev | sort | uniq -c
python3 moncode.py < test1.txt

-->

## Rediriger la sortie d'une commande sur l'entrée d'une autre avec le _pipe_ '|'

On peut rediriger la sortie d'une commande sur l'entrée d'une autre, plusieurs
fois d'affilée si on veut, par exemple :

```bash
getent passwd | grep nicolas
```

**exercices:**

* Que fait le programme suivant ? (testez morceau par morceau en rajoutant une commande à chaque fois)
  ```bash
  ls | rev | cut -s -d'.' -f1 | rev | sort | uniq -c
  ```
* On peut afficher un fichier avec `cat NOMDUFICHIER`. Comment afficher seulement les lignes qui contiennent
le mot "renard" ? Comment afficher seulement les lignes qui contiennent "renard" et "ami" ?

## Entrée standard, sortie standard, erreur standard (stdin,stdout,stderr)

Ce qu'on redirige avec le _pipe_, c'est l'entrée standard qui se comporte comme un fichier sur lequel lit
un programme, et la sortie standard, qui est comme un fichier sur lequel le programme écrit.

Par exemple en python, `print` affiche sur la sortie standard et `input` lit sur l'entrée standard.

Les erreurs sont en général affichées sur l'erreur standard, qui s'affiche aussi dans le terminal mais
peut être manipulée différemment.

Seule la sortie standard est redirigée par le _pipe_, les erreurs ne sont donc pas envoyées au programme suivant.

## Rediriger la sortie vers un fichier, ou l'entrée depuis un fichier

On peut rediriger la sortie standard d'une commande vers un fichier avec `>`:
```bash
ls > resultats_de_ls.txt
```
Ou rediriger l'entrée standard depuis un fichier avec `<`:
```
tr 'A-Za-z' 'N-Z-A-Mn-z-a-m' < fichier_secret_a_chiffrer.txt
```
Voire les deux:
```
tr 'A-Za-z' 'N-Z-A-Mn-z-a-m' < fichier_secret_a_chiffrer.txt > message_secret.txt
```

Si on utilise `>>`, cela rajoute le texte à la fin du fichier (Attention, `>` écrase le fichier
s'il existait déjà !).

**exercice :**
C'est une bonne manière de tester vos programmes python qui utilisent input() et print().
Reprenez un exercice de france-ioi que vous avez réussi, copiez une des entrées fournies
dans un fichier au même endroit, et tester le résultat avec `python3 votrecode.py < test.txt`.

L'erreur standard peut être redirigée indépendamment en mettant `2` devant le `>`:
```
ls 2> erreurs_de_ls.txt
```

## Les fichiers spéciaux ̀/dev/null`, `/dev/random`

* `/dev/null` est un fichier linux qui jette tout ce qu'on lui envoie.

On peut s'en servir pour ne pas afficher la sortie ou l'erreur standard, par exemple:

```
find /
```
renvoie beaucoup d'erreurs car on n'a pas les droits sur tous les fichiers.

```
find / 2>/dev/null
```
n'affiche pas les erreurs.

* `/dev/random` est un fichier depuis lequel on peut lire et qui produit à la demande
du contenu aléatoire.
