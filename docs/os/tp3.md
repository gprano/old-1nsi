# Permissions des fichiers

## Rappel sur le terminal

* Page de manuel d'une commande : `man ls` (quitter avec la touche Q)  
  Ou en plus résumé : `tldr ls`

## Utilisateurs et groupes

* Pour savoir quel utilisateur vous êtes : `whoami`
* Pour savoir à quel groupes vous appartenez : `groups`
* Pour se connecter comme un autre utilisateur : `su nom_utilisateur`
* Pour exécuter une commande avec les permissions du super-utilisateur (root) :
`sudo commande`

!!! list "À faire"
    Trouver votre nom d'utilisateur et les groupes auxquels vous appartenez.  
    Essayez de vous connecter avec le pseudo de la personne à côté de vous.  
    Essayez de lancer une commande comme super-utilisateur.
    Vous pouvez voir la liste de tous les utilisateurs du lycée avec `getent passwd`
    et de tous les groupes avec `getent group`

## Permissions des fichiers

* `ls -l` affiche plus de détails sur les dossiers et fichiers du répertoire
  où vous êtes actuellement.
  ```bash
  gprano@jalan:/etc$ ls -l
  drwxr-xr-x  8 root     root       4096 15 mars   2021  ppp
  lrwxrwxrwx  1 root     root         18 10 sept. 17:32  printcap -> /run/cups/printcap
  -rw-r--r--  1 root     root        769 10 avril  2021  profile
  ```
    * le premier caractère indique que ppp est un dossier (d), printcap un lien
      symbolique (l) et profile un fichier (-)
    * tous sont possédés par l'utilisateur `root` et le groupe `root`
    * la taille du fichier `profile` est 769 octets
    * la date affichée est celle de la dernière modification
* les **permissions** d'un fichier ou dossier sont données par les chaînes
`rwxr-xr-x`,`rw-r--r--`,...
    * les trois premiers caractères représentent ce que peut faire l'**utilisateur**
      qui possède le fichier, les trois suivants ce que peuvent faire les **membres
      du groupe** qui possède le fichier, les trois derniers ce que peut faire
      **n'importe quel utilisateur**.
    * `r` indique la permission de **lire** le fichier  
      `w` indique la permission d'**écrire** (modifier) le fichier  
      `x` indique la permission d'**exécuter** le fichier (si c'est un programme).
    * pour un dossier, `r` permet de lister le contenu, `x` de chercher les 
      méta-données et le contenu des fichiers/dossiers inclus, et `w` de 
      créer/supprimer/renommer des fichiers inclus.
    * on note parfois les permissions par la valeur correspondant au nombre binaire
      sur 3 bits :
      
      | permissions | valeur binaire | valeur décimale |
      | --- | --- | --- |
      | --- | 000 | 0 |
      | r-- | 100 | 4 |
      | rw- | 110 | 6 |
      | rwx | 111 | 7 |
      | r-x | 101 | 5 |
    
    * et du coup avec trois chiffres on peut donner les permissions d'un fichier:
    
      | permissions | valeur décimale | autorisations |
      | --- | --- | --- |
      | `rwxrwxrwx` | 777 | tout est autorisé pour tout le monde |
      | `rwx------` | 700 | seul l'utilisateur peut (tout) faire |
      | `rwxr--r--` | 744 | l'utilisateur peut tout faire, les autres lire seulement |
      | `rwxr-xr-x` | 755 | tout le monde peut lire/exécuter, seul l'utilisateur peut modifier |
      | 

!!! list "À faire"
    * Quelle sont les permissions de `/etc/ssh/ssh_host_rsa_key` ?
      Que se passe-t-il si vous essayez d'en afficher le contenu avec `cat` ?
    * Quelle sont les permissions de `/etc/crontab` ?
      Que se passe-t-il si vous essayez d'en afficher le contenu avec `cat` ?
    * Quelles sont les permissions de `/etc/sudoers` (qui contient la liste
      des utilisateurs qui ont le droit d'utiliser `sudo`) ?
      Pouvez-vous savoir ce qu'il y a dedans ?
    
## Modifier l'appartenance et les permissions

Modifier l'utilisateur qui possède un fichier : `chown utilisateur fichier`

Modifier aussi le groupe : `chown utilisateur:groupe fichier`

Mettre les permissions d'un fichier à 755 : `chmod 755 fichier`

On peut utiliser l'option `-R` pour faire la modification **R**écursivement, c'est à dire dans tous les sous-fichiers et sous-dossiers d'un répertoire.

!!! list "À faire"
    * Par défaut, les fichiers dans votre répertoire personnel sont lisibles par tout le monde. Créez un fichier texte nommé "secret.txt", écrivez un mot dedans, et rendez le lisible par vous uniquement. Appelez-moi pour que je vérifie que je n'arrive pas à le lire depuis mon ordinateur.

## Se connecter sur un autre ordinateur avec `ssh`

`ssh utilisateur@machine` permet de se connecter à un autre ordinateur par le réseau.

`machine` peut être une adresse IP ou bien un nom de machine (qui sera traduit en adresse IP avec une requête DNS).

!!! list "À faire"
    Essayer de vous connecter sur l'ordinateur de la personne à côté de vous.  
    Pouvez-vous lire son répertoire personnel ?  
    Pouvez-vous rajouter un fichier dedans ? (avec `touch`)
    
!!! info
    ssh veut dire **secure shell**, c'est à dire qu'il permet d'ouvrir un
    terminal sur la machine distante de manière sécurisée parce que les données
    transmises sont chiffrées.
    
    Pour que ça fonctionne, il faut qu'un serveur ssh soit lancé sur la machine
    distante et attende les connections (sur le port TCP 22 par défaut).
    
    Il est possible de configurer le serveur ssh dans le fichier `/etc/ssh/sshd_config`
    pour par exemple changer le port utilisé, refuser certaines connections, etc.
    Un serveur ssh ouvert constitue une porte d'entrée sur la machine et peut
    donc être source de vulnérabilité.
    
    Sur un serveur on peut observer les tentatives d'intrusion par ssh qui ont
    échoué dans le fichier de log `/var/log/auth.log`
    
!!! info
    Les attaques de type "élévation de privilèges" (privilege escalation) utilisent
    des failles pour obtenir les droits du super-utilisateur.
    
    Une [faille](https://cyberveille-sante.gouv.fr/cyberveille/2943-vulnerabilite-dans-polkit-pkexec-de-linux-2022-01-27) dans la commande `pkexec` qui permet de devenir root sur toute
    machine linux a été découverte il y a quelques mois.

    Si les ordinateurs de la salle n'ont pas été mis à jour depuis avec le correctif,
    cela veut dire que vous pouvez essayer de l'exploiter pour devenir root...
